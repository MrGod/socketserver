FROM node:alpine

WORKDIR /apps
RUN npm install ws node-static
#ADD index.html /apps/
ADD server.js /apps/

ENTRYPOINT ["node", "server.js"]

