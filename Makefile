init:
	docker-compose build && make start

start:
	docker-compose up -d && docker-compose ps && docker-compose logs -f

stop:
	docker-compose down && docker-compose ps
