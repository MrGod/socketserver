# Node.js WebSocket example in Docker

This is a small demo which demonstrates the use of WebSocket in Docker.

## Steps

These are the steps to be executed in order to start the demo.

### Build the image

~~~~
docker-compose build
~~~~

### Start the container


~~~~
docker-compose up -d
~~~~
