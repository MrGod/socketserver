// Source: https://github.com/einaros/ws
const http = require('http');
const WebSocket = require('ws');

const server = http.createServer();
const wss = new WebSocket.Server({ noServer: true });
// const wss = new WebSocket.Server({port: 5000});

function format(value) {
    return ("0" + value).slice(-2);
}

function getDate() {
    let today = new Date();
    let time = format(today.getHours()) + ":" + format(today.getMinutes()) + ":" + format(today.getSeconds());

    return today.getFullYear() + "." + format(today.getMonth() + 1) + "." + format(today.getDate()) + " " + time;
}

function log(data) {
    console.log(getDate(), data);
}

function sendData(socket, data) {
    socket.send(data);
    log(' <<< ' + data);
}

wss.on('connection', function (ws, req) {
    console.log(req.headers);
    log('Client connection: ' + ws.clientId);
    log('Authorization: ' + (req.headers.hasOwnProperty('authorization') ? req.headers['authorization'] : 'not found'));
    sendData(ws, 'Hi!');
    ws.on('message', function (message) {
        log(' >>> ' + message);
        sendData(ws, message);
    });

    // ws.on('disconnect', function (ws) {
    //     log('Client disconnect: ' + ws.clientId);
    // });
});

server.on('upgrade', function(req, socket, head) {
    console.log(req.headers);
    wss.handleUpgrade(req, socket, head, function done(ws) {
        wss.emit('connection', ws, req);
    });
});

server.listen(5000);
